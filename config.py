import os

# User space BEGIN

BUILD_LEVEL = ''

TARGET_PLATFORM = 'linux'

LINK_SCRIPT = '' 

TARGET_NAME = 'program'

MY_EXEC_PATH = ''

# User space END

#********************************************************************#
# Don't change the followings unless you know what you are doing
#********************************************************************#

TARGET = TARGET_NAME + '.elf'
HEX_TARGET = TARGET_NAME + '.hex'
BIN_TARGET = TARGET_NAME + '.bin'

# internal var
target_platform = TARGET_PLATFORM
compiler_prefix = ''
link_script = ''

# compiler related

CDEFINES = []

if target_platform == 'cortex-m3':
    compiler_prefix = 'arm-none-eabi-'
    link_script = LINK_SCRIPT
    #specific
    DEVICE = ' -mcpu=cortex-m3 -mthumb -ffunction-sections -fdata-sections'
    CCFLAGS = DEVICE + ' -Dgcc' + ' --specs=nano.specs'
    CPPDEFINES = CDEFINES
    ASFLAGS = ' -c' + DEVICE + ' -x assembler-with-cpp -Wa,-mimplicit-it=thumb '
    LINKFLAGS = DEVICE  + ' --specs=nano.specs' + ' -Wl,--gc-sections,-Map=target.map,-cref,-u,Reset_Handler' +  ' -T %s' % link_script

    LIBPATH = ''
    LIBS = []

    if BUILD_LEVEL == 'debug':
        CCFLAGS += ' -O0 -gdwarf-2 -g'
        ASFLAGS += ' -gdwarf-2'
    else:
        CCFLAGS += ' -Os'

    CXXFLAGS = CCFLAGS 

elif target_platform == 'linux':
    compiler_prefix = ''
    link_script = '/home/vano/all/Desktop/latest/ua/tests/linux/link.lds'
    #specific
    CCFLAGS = ' -Dgcc' 
    CPPDEFINES = []
    ASFLAGS = ''
    LINKFLAGS = ' -Wl,-Map=target.map'  +  ' -W %s' % link_script
    # LINKFLAGS += ' %s' %link_script
    LIBPATH = 'libs'
    LIBS = ['ulog']

    if BUILD_LEVEL == 'debug':
        CCFLAGS += ' -O0 -gdwarf-2 -g'
        ASFLAGS += ' -gdwarf-2'
    else:
        CCFLAGS += ' -O0'

    CXXFLAGS = CCFLAGS
else:
    print("Unknown target-platform (%s)" %{target_platform})
    os.abort()

CC = compiler_prefix + 'gcc'
AS = compiler_prefix + 'gcc'
AR = compiler_prefix + 'ar'
CXX = compiler_prefix + 'g++'
LINK = compiler_prefix + 'gcc'
SIZE = compiler_prefix + 'size'
OBJDUMP = compiler_prefix + 'objdump'
OBJCPY = compiler_prefix + 'objcopy'

#
EXEC_PATH = MY_EXEC_PATH

#