#include <stdio.h>

#define LOG_TAG "main"
#include <elog.h>

static void init_elog(void)
{
  elog_init();
  /* set EasyLogger log format */
  elog_set_fmt(ELOG_LVL_ASSERT, ELOG_FMT_ALL);
  elog_set_fmt(ELOG_LVL_ERROR, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
  elog_set_fmt(ELOG_LVL_WARN, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
  elog_set_fmt(ELOG_LVL_INFO, ELOG_FMT_LVL | ELOG_FMT_TAG | ELOG_FMT_TIME);
  elog_set_fmt(ELOG_LVL_DEBUG, ELOG_FMT_ALL & ~(ELOG_FMT_FUNC | ELOG_FMT_T_INFO | ELOG_FMT_P_INFO));
  elog_set_fmt(ELOG_LVL_VERBOSE, ELOG_FMT_ALL & ~(ELOG_FMT_FUNC | ELOG_FMT_T_INFO | ELOG_FMT_P_INFO));

#ifdef ELOG_COLOR_ENABLE
    elog_set_text_color_enabled(true);
#endif    

  /* start EasyLogger */
  elog_start();
}

static void test_elog()
{
    log_a("hi");
    log_e("hi");
    log_d("hi");
    log_i("hi");
    log_v("hi");
    log_w("hi");
}

int main()
{
    init_elog();
    test_elog();
}